import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import ProductList from "../products/ProductList";
import Cart from "../cart/Cart";
import productData from "../products/model";

class App extends Component {
  constructor(props) {
    super(props);
    let initialProducts = JSON.parse(
      JSON.parse(localStorage.getItem("products"))
    );
    let initialCartItems = JSON.parse(
      JSON.parse(localStorage.getItem("cartItems"))
    );
    this.addToCart = this.addToCart.bind(this);
    this.state = {
      cartItems: initialCartItems || [],
      formattedCartItems: [],
      products: initialProducts
    };
    this.formatCartItems();
  }

  addToCart = product => {
    this.state.cartItems.push({
      name: product.name,
      price: product.price
    });
    this.setState({
      cartItems: this.state.cartItems
    });

    this.formatCartItems();
    localStorage.setItem(
      "cartItems",
      JSON.stringify(JSON.stringify(this.state.cartItems))
    );
  };

  formatCartItems = () => {
    let theFormattedCartItems = [];
    this.state.cartItems.map(item => {
      if (!theFormattedCartItems.some(e => e.name === item.name)) {
        theFormattedCartItems.push({
          name: item.name,
          quantity: 1,
          price: item.price
        });
      } else {
        let a = theFormattedCartItems.find(z => z.name === item.name);
        a.quantity++;
      }
    });
    this.setState({
      formattedCartItems: theFormattedCartItems
    });
  };

  removeFromCart = product => {
    const index = this.state.cartItems
      .map(e => {
        return e.name;
      })
      .indexOf(product.name);
    if (index > -1) {
      this.state.cartItems.splice(index, 1);
    }
    this.setState({
      cartItems: this.state.cartItems
    });
    this.formatCartItems();
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className="App-header-container">
            <img
              src="https://www.ezyvet.com/wp-content/uploads/2016/04/ezyVet-Logo-22-4.png"
              alt="ezyVet Cloud Veterinary Practice Management Software"
              id="logo"
              className="App-logo"
              data-height-percentage="65"
              data-actual-width="768"
              data-actual-height="252"
            />
          </div>
        </header>
        <div
          style={{ display: "flex", flexDirection: "column" }}
          className="App-header-container"
        >
          <ProductList
            products={this.state.products}
            addToCart={this.addToCart}
          />
          <Cart
            products={this.state.formattedCartItems}
            removeFromCart={this.removeFromCart}
          />
        </div>
      </div>
    );
  }
}

export default App;
