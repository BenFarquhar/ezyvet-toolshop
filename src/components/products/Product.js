import React from "react";

export default function Product(props) {
  return (
    <tr>
      <td>{props.product.name}</td>
      <td>{parseFloat(props.product.price).toFixed(2)}</td>
      <td>
        <button onClick={() => props.addToCart(props.product)}>
          Add to Cart
        </button>
      </td>
    </tr>
  );
}
