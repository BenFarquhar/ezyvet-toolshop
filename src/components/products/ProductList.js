import React, { Component } from "react";
import Product from "./Product";

class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const listItems = this.props.products.map(product => (
        <Product product={product} addToCart={this.props.addToCart}/>
    ));
    return (
      <table style={{width: '100%', marginBottom: '20px', marginTop: '20px'}}>
      <caption>Shop Products</caption>
      <tbody>

      
        <tr>
          <th>Name</th>
          <th>Price</th>
          <th>Actions</th>
        </tr>
        {listItems}
        </tbody>
      </table>
    );
  }
}

export default ProductList;
