import React, { Component } from "react";
import CartItem from "./CartItem";

class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const divStyle = {
      width: "100%"
    };

    const listItems = this.props.products.map(product => (
      <CartItem product={product} removeFromCart={this.props.removeFromCart} />
    ));
    return (
      <table style={{width: '100%'}}>
        <caption>Shopping Cart</caption>
        <tbody>

        <tr>
          <th>Name</th>
          <th>Price</th>
          <th>Quantity</th>
          <th>Total</th>
          <th>Actions</th>
        </tr>
        {listItems}

        </tbody>
      </table>
    );
  }
}

export default Cart;
