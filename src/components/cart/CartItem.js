import React from 'react';

export default function CartItem(props) {
  return (
    <tr>
        <td>{props.product.name}</td>
        <td>{parseFloat(props.product.price).toFixed(2)}</td>
        <td>{props.product.quantity}</td>
        <td>{parseFloat(props.product.quantity * props.product.price).toFixed(2)}</td>
        <td>
          <button onClick={() => props.removeFromCart(props.product)}>
            Remove from Cart
          </button>
        </td>
    </tr>
  )
}
